﻿using UnityEngine;
using System.Collections;

public class WakeZombies : MonoBehaviour {

	public ZombieMove[] zombies;


	public void OnTriggerEnter(Collider collider) {
		for (int i = 0; i < zombies.Length; i++) {
			zombies[i].Wake();
		}		
	}

}
